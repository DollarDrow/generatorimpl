package ru.generator;

import java.util.Random;
/**
 * Created by DOLLARDROW on 08.08.2016.
 */
public class GeneratorEn implements Generator   {

    String[] verbsEn = {"walk!", "smoke!", "win!",  "reed!", "kill!",
            "run!", "swim!", "fly!"};

    String[] nounsEn = {"You ", "We ", "He ", "She ", "They ",  "I ", "That "};

    String[] adjectivesEn = {"have a difficult ", "have a small ", "have a late ",
            "have a lot ", "have a hot ", "have a nice", "have a easy ", "have a short "};

    public String generateWords(){
        int verbsREn = new Random().nextInt(verbsEn.length);
        String randomVerbEn = (verbsEn[verbsREn]);

        int nounsREn = new Random().nextInt(nounsEn.length);
        String randomNounEn = (nounsEn[nounsREn]);

        int adjectivesREn = new Random().nextInt(adjectivesEn.length);
        String randomAdjectiveEn = (adjectivesEn[adjectivesREn]);

        return String.format(  randomNounEn + randomAdjectiveEn + randomVerbEn);
    }

}
