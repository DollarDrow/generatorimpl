package ru.generator;

import java.util.Random;
/**
 * Created by DOLLARDROW on 08.08.2016.
 */
public class GeneratorMat implements Generator {

    String[] verbsMat = {"затрахал ", "заебал ", "допизделся ", "довыебывался ",
            "напизделся ", "съебал ", "набздел ", "охуел ", "расхуярился ",  "уебался "};

    String[] nounsMat = {"Пиздюк, ", "Долбоеб, ", "Дуроеб, ", "Блядь, ",
            "Распиздяй, ", "Рапизденыш, ", "Хуйло, ", "Хуй, ","Хуила, ", "Резиноеб, "};

    String[] adjectivesMat = {"охуенною", "ебануто." ,"заебато.",
            "коноебнуто.", "выебнуто.", "блядски.", "распиздяйски.",  "хуево.",
            "петушино.", "по шалавски"};

    public String generateWords(){
        int verbsRMat = new Random().nextInt(verbsMat.length);
        String randomVerbMat = (verbsMat[verbsRMat]);

        int nounsRMat = new Random().nextInt(nounsMat.length);
        String randomNounMat = (nounsMat[nounsRMat]);

        int adjectivesRMat = new Random().nextInt(adjectivesMat.length);
        String randomAdjectiveMat = (adjectivesMat[adjectivesRMat]);

        return String.format(  randomNounMat + randomVerbMat+ randomAdjectiveMat);
    }

}
