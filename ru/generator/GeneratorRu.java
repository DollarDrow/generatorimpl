package ru.generator;

import java.util.Random;
/**
 * Created by dollardrow on 28.07.16.
 */
public class GeneratorRu implements Generator {

    String[] verbsRu = {"Бежишь, ", "Скачешь, ", "Красуешься, ",
            "Любуешься, ", "Ненавидишь, ", "Качаешься, ", "Летишь, ",
            "Плывешь, ", "Катаешься, ", "Стремишься, "};

    String[] nounsRu = {"зверь, ", "заяц, ", "волк, ",
            "лев, ", "попугай, ", "крыса, ",
            "петух, ", "курица, ", "птенец, ", "собака, "};

    String[] adjectivesRu = {"элегантно", "красиво", "быстро",
            "медленно", "невзрачно",
            "ревностно", "беззаботно", "неуклюжо",
            "восхитительно", "ярко"};

    public String generateWords(){
        int verbsRRu = new Random().nextInt(verbsRu.length);
        String randomVerbRu = (verbsRu[verbsRRu]);

        int nounsRRu = new Random().nextInt(nounsRu.length);
        String randomNounRu = (nounsRu[nounsRRu]);

        int adjectivesRRu = new Random().nextInt(adjectivesRu.length);
        String randomAdjectiveRu = (adjectivesRu[adjectivesRRu]);

        return String.format( randomVerbRu + randomNounRu + randomAdjectiveRu);
    }

}